package de.vdw.io.alpha2mqtt.models;

import lombok.Value;

@Value
public class Credentials {
  String username, password;

}
